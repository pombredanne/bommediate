var assert = require("assert");
var setImmediate = require("../lib/setImmediate").setImmediate;
var clearImmediate = require("../lib/setImmediate").clearImmediate;

suite("setImmediate", function() {
    "use strict";

    test("should run", function(done) {
        setImmediate(function() {
            done();
        });
    });

    test("should run in order", function(done) {
        var array = [];

        setImmediate(function() {
            array.push(1);
        });
        setImmediate(function() {
            array.push(2);
        });
        setImmediate(function() {
            assert.deepEqual(array, [1, 2]);
            done();
        });
    });

    test("should not block the event loop indefinitely", function(done) {
        var handle = null;

        var x = function() {
            handle = setImmediate(x);
        };
        x();

        setTimeout(function() {
            handle.destroy();
            done();
        }, 0);
    });

    test("should pass 0 arguments to the function when called with 0 arguments", function(done) {
        setImmediate(function() {
            assert.strictEqual(arguments.length, 0);
            done();
        });
    });

    test("should pass 1 argument to the function when called with 1 argument", function(done) {
        setImmediate(function(first) {
            assert.strictEqual(first, 1000);
            assert.strictEqual(arguments.length, 1);
            done();
        }, 1000);
    });

    test("should pass N arguments to the function when called with N arguments", function(done) {
        setImmediate(function() {
            assert.strictEqual(arguments.length, 10);
            for (var i = 0; i < 10; i++) {
                assert.strictEqual(arguments[i], 1000 + i);
            }
            done();
        }, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009);
    });
});

suite("setImmediate(...).destroy()", function() {
    "use strict";

    test("should cancel execution", function(done) {
        var called = false;
        var handle = setImmediate(function() {
            called = true;
        });
        handle.destroy();
        setImmediate(function() {
            assert.strictEqual(called, false);
            done();
        });
    });

    test("should allow calling multiple times", function() {
        var handle = setImmediate(function() {});
        handle.destroy();
        handle.destroy();
    });

    test("should return true if the execution could be cancelled", function() {
        var handle = setImmediate(function() {});
        assert.strictEqual(handle.destroy(), true);
    });

    test("should return false if the execution could not be cancelled", function(done) {
        var handle = setImmediate(function() {});
        setImmediate(function() {
            assert.strictEqual(handle.destroy(), false);
            done();
        });
    });

    test("should return false mid-execution", function(done) {
        var handle = setImmediate(function() {
            assert.strictEqual(handle.destroy(), false);
            done();
        });
    });

    test("should cancel until the execution has started", function(done) {
        var array = [];
        var handle = null;

        setImmediate(function() {
            handle.destroy();
            array.push(1);
        });
        handle = setImmediate(function() {
            array.push(2);
        });
        setImmediate(function() {
            assert.deepEqual(array, [1]);
            done();
        });
    });

    test("should not do anything if called mid-execution", function(done) {
        var array = [];
        var handle = null;

        setImmediate(function() {
            array.push(1);
        });
        handle = setImmediate(function() {
            handle.destroy();
            array.push(2);
        });
        setImmediate(function() {
            assert.deepEqual(array, [1, 2]);
            done();
        });
    });
});

suite("setImmediate error handling", function() {
    "use strict";

    var TestError = function() {};
    TestError.prototype = new Error();

    var errors = [];
    var listeners = [];

    beforeEach(function() {
        listeners = process.listeners("uncaughtException");
        process.removeAllListeners("uncaughtException");

        process.on("uncaughtException", function(err) {
            if (err instanceof TestError) {
                errors.push(err);
            } else {
                listeners.forEach(function(listener) {
                    listener(err);
                });
            }
        });
    });

    afterEach(function() {
        process.removeAllListeners("uncaughtException");
        listeners.forEach(function(listener) {
            process.on("uncaughtException", listener);
        });
        errors = [];
        listeners = [];
    });

    test("should allow other calls to run despite an error", function(done) {
        setImmediate(function() {
            throw new TestError();
        });

        setImmediate(function() {
            done();
        });
    });

    test("should propagate errors to the main handler", function(done) {
        var error = new TestError();

        setImmediate(function() {
            throw error;
        });

        setImmediate(function() {
            assert.strictEqual(errors.length, 1);
            assert.strictEqual(errors[0], error);
            done();
        });
    });
});

suite("clearImmediate", function() {
    "use strict";

    test("should accept any value without throwing", function() {
        clearImmediate();
        clearImmediate(null);
        clearImmediate({});
    });

    test("should return undefined", function() {
        var handle = setImmediate(function() {});
        assert.strictEqual(typeof clearImmediate(handle), "undefined");
    });
});
