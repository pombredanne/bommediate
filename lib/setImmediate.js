(function(func) {
    "use strict";

    if (typeof define === "function" && define.amd) {
        define(func);
    } else if (typeof module === "object" && typeof module.exports === "object") {
        module.exports = func();
    } else {
        self.bommediate = func();
    }
}(function() {
    "use strict";

    var _window = typeof window === "undefined" ? null : window;
    var _self = typeof self === "undefined" ? null : self;
    var _global = _window || _self || (typeof global === "undefined" ? null : global);
    var _now = Date.now;

    var attempt = function(func, args) {
        try {
            if (args === null) {
                func();
            } else if (args.length === 1) {
                func(args[0]);
            } else {
                func.apply(null, args);
            }
        } catch (err) {
            return {
                "error": err
            };
        }
        return null;
    };

    var options = [
        function(func) {
            if (!_global || typeof _global.setImmediate !== "function") {
                return null;
            }

            var nativeSetImmediate = _global.setImmediate;
            return function() {
                nativeSetImmediate(func);
            };
        },

        function(func) {
            if (!_window || typeof _window.postMessage !== "function") {
                return null;
            }

            var uid = "immediate-" + Math.random();

            _window.addEventListener("message", function(event) {
                if (event.data === uid) {
                    func();
                }
            }, false);

            return function() {
                _window.postMessage(uid, "*");
            };
        },

        function(func) {
            var MessageChannel = _global && _global.MessageChannel;
            if (!MessageChannel) {
                return null;
            }

            var channel = new MessageChannel();
            channel.port1.onmessage = function() {
                func();
            };
            return function() {
                channel.port2.postMessage("");
            };
        }
    ];

    var Immediate = function(func, args, prev, next) {
        this._func = func;
        this._args = args;
        this._done = false;

        this._prev = prev;
        this._next = next;
    };

    Immediate.prototype.destroy = function() {
        if (this._done) {
            return false;
        }

        this._func = null;
        this._args = null;
        this._done = true;

        var prev = this._prev;
        var next = this._next;
        this._prev = null;
        this._next = null;

        if (next !== null) {
            next._prev = prev;
        }
        if (prev !== null) {
            prev._next = next;
        }
        return true;
    };

    var performing = false;
    var head = new Immediate(null, null, null, null);
    var tail = new Immediate(null, null, null, null);
    head._next = tail;
    tail._prev = head;

    var isEmpty = function() {
        return head._next === tail;
    };

    var append = function(func, args) {
        var immediate = new Immediate(func, args, tail._prev, tail);
        tail._prev._next = immediate;
        tail._prev = immediate;
        return immediate;
    };

    var iterate = function() {
        var err = null;
        var marker = append(null, null);

        while (err === null) {
            var next = head._next;
            if (next === marker) {
                break;
            }

            var func = next._func;
            var args = next._args;
            next.destroy();
            err = attempt(func, args);
        }

        marker.destroy();
        return err;
    };

    var findImmediateImplementation = function(_options, callback) {
        for (var i = 0, len = _options.length; i < len; i++) {
            var option = _options[i];
            var implementation = option(callback);
            if (implementation !== null) {
                return implementation;
            }
        }

        var setTimeout = _global.setTimeout;
        return function() {
            setTimeout(callback, 0);
        };
    };

    var setImmediateImplementation = findImmediateImplementation(options, function() {
        if (performing) {
            return;
        }
        performing = true;

        var err = null;
        var start = _now();
        while (!isEmpty()) {
            err = iterate();
            if (err !== null) {
                break;
            }

            var now = _now();
            if (now < start || now - start >= 4) {
                break;
            }
        }

        performing = false;
        if (!isEmpty()) {
            setImmediateImplementation();
        }
        if (err) {
            throw err.error;
        }
    });

    var setImmediate = function(func) {
        var args = null;
        var argsLength = arguments.length - 1;
        if (argsLength > 0) {
            args = new Array(argsLength);
            for (var i = 0; i < argsLength; i++) {
                args[i] = arguments[i + 1];
            }
        }

        if (!performing && isEmpty()) {
            setImmediateImplementation();
        }
        return append(func, args);
    };

    setImmediate.setImmediate = setImmediate;

    setImmediate.clearImmediate = function(immediate) {
        if (immediate instanceof Immediate) {
            immediate.destroy();
        }
    };

    return setImmediate;
}));
